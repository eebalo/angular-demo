const puppeteer = require('puppeteer');
process.env.CHROME_BIN = puppeteer.executablePath();module.exports = function (config) {
  config.set({
    customLaunchers: {
      ChromeHeadlessNoSandbox: {
        base: 'ChromeHeadless',
        flags: ['--no-sandbox']
      }
    },
  });
};